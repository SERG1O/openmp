#include <stdio.h>
#include <stdlib.h>
#include <omp.h>


void matriz_vector_serial(double **A,double* x, double* y, int m, int n){
  int i,j;
    for (i = 0; i<m; i++){
      y[i] = 0.0;
      //printf("%d,%d,%d\n",i,m,n);
      for (j=0;j<n;j++){

          y[i]+= A[i][j]*x[j];
          //printf("%f\n",y[i]);
        }
      }
  return;
}

void matriz_vector(double **A,double* x, double* y, int m, int n, int thread_count){
  int i,j;
  #pragma omp parallel for num_threads(thread_count)\
    default(none) private(i,j) shared(A,x,y,m,n)
    for (i = 0; i<m; i++){
      y[i] = 0.0;
      //printf("%d,%d,%d\n",i,m,n);
      for (j=0;j<n;j++){

          y[i]+= A[i][j]*x[j];
          //printf("%f\n",y[i]);
        }
      }
  return;
}


int main(int argc, char **argv)
{  
  int m = 8000000;
  int n = 8;
  double  **A;
  int i,j;
  double *x, *y; 
  double start, finish;
  x = (double*)malloc(n*sizeof(double));
  y = (double*)malloc(m*sizeof(double)); 

  for(i=0; i<n; i++) x[i] = 1;

  //int **A = malloc(n*sizeof(int*) );
  A  = new double* [m]; 
  for(i=0; i<m; i++){
     //A[i] =  malloc(8*sizeof(int));
     A[i] = new double [n];
    for (j=0; j<n;j++){
      A[i][j] = 1 ;
    }
  }
  start = omp_get_wtime();
  matriz_vector(A,x,y,m,n,1);
  finish = omp_get_wtime();
  printf("Tiempo transcurrido multiplicacion matriz_vector %.2f second\n",finish-start);

  printf("Valor maximo %.2f \n",y[m-1]);


}