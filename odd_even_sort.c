#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void odd_even_sort_serial(int *a, int n) {
 int i, fase, tmp;
 for (fase = 0; fase < n; fase++)
 	if (fase % 2 == 0) {
 	for (i = 1; i < n; i += 2)
 		if (a[i - 1] > a[i]) {
			 tmp = a[i];
			 a[i] = a[i - 1];
			 a[i - 1] = tmp;
			 }
	 } else {
	 for (i = 1; i < n - 1; i += 2)
		 if (a[i] > a[i + 1]) {
		 tmp = a[i];
		 a[i] = a[i + 1];
		 a[i + 1] = tmp;
		 }
	 }
}



void odd_even_sort_parallel1(int *a, int n, int thread_count) {
int i, fase, tmp;
 for (fase = 0; fase < n; fase++)
 	if (fase % 2 == 0) {
#pragma omp parallel for num_threads(thread_count) default(none) \
 	shared(a, n) private(i, tmp)
 	for (i = 1; i < n; i += 2)
 		if (a[i - 1] > a[i]) {
			 tmp = a[i];
			 a[i] = a[i - 1];
			 a[i - 1] = tmp;
 		}
 	} else {
#pragma omp parallel for num_threads(thread_count) default(none) \
	 shared(a, n) private(i, tmp)
	 for (i = 1; i < n - 1; i += 2)
		 if (a[i] > a[i + 1]) {
			 tmp = a[i];
			 a[i] = a[i + 1];
			 a[i + 1] = tmp;
		}
	 }
}

void odd_even_sort_parallel2(int *a, int n, int thread_count) {
int i, fase, tmp;
#pragma omp parallel num_threads(thread_count) default(none)\
	shared(a,n) private(i,tmp,fase)
 for (fase = 0; fase < n; fase++)
 	if (fase % 2 == 0) {
#pragma omp for
 	for (i = 1; i < n; i += 2)
 		if (a[i - 1] > a[i]) {
			 tmp = a[i];
			 a[i] = a[i - 1];
			 a[i - 1] = tmp;
 		}
 	} else {
#pragma omp for
	 for (i = 1; i < n - 1; i += 2)
		 if (a[i] > a[i + 1]) {
			 tmp = a[i];
			 a[i] = a[i + 1];
			 a[i + 1] = tmp;
		}
	 }
}


int main(int argc, char* argv[]){
	int i, n = 100000;
	int a[n], c[n];
	double start, finish; 
	srand(12345L);


	for(i = 0; i<n; i++){
		a[i] = c[i] = rand()%1000000;
	}
	start = omp_get_wtime();
	//odd_even_sort_serial(a,n);
	finish = omp_get_wtime();
	printf("Tiempo transcurrido odd-even-sort serial %.2f second\n",finish-start);


	for(i = 0; i<n; i++){
		a[i] = c[i];
	}
	start = omp_get_wtime();
	odd_even_sort_parallel1(a,n,4);
	finish = omp_get_wtime();
	printf("Tiempo transcurrido odd-even-sort paralelo %.2f second\n",finish-start);

	for(i = 0; i<n; i++){
		a[i] = c[i];
	}
	start = omp_get_wtime();
	odd_even_sort_parallel2(a,n,4);
	finish = omp_get_wtime();
	printf("Tiempo transcurrido odd-even-sort paralelo2 %.2f second\n",finish-start);


}
